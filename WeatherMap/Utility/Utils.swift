//
//  Utils.swift
//  WeatherMap
//
//  Created by Saba Khutsishvili on 10/7/19.
//

import UIKit

func instantiate<T: UIViewController>(_ viewController: T.Type, from storyboard: String) -> T {
    let storyboard = UIStoryboard(name: storyboard, bundle: nil)
    let controller = storyboard.instantiateViewController(withIdentifier: viewController.className)
    return controller as! T
}
