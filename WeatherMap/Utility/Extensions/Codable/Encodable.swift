//
//  Encodable.swift
//  WeatherMap
//
//  Created by Saba Khutsishvili on 10/9/19.
//

import Foundation

extension Encodable {
    
    var toData: Data? {
        return try? JSONEncoder().encode(self)
    }
    
    var toJSON: [String: Any]? {
        guard let data = toData else { return nil }
        if let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) {
            return json as? [String: Any]
        }
        return nil
    }
}
