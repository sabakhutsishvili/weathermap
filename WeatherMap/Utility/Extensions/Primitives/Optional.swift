//
//  Optional.swift
//  WeatherMap
//
//  Created by Saba Khutsishvili on 10/7/19.
//

import Foundation

extension Optional {
    
    func or(_ default: Wrapped) -> Wrapped {
        return self ?? `default`
    }
    
    func unwrap(_ do: (Wrapped) -> ()) {
        guard let on = self else { return }
        `do`(on)
    }
}
