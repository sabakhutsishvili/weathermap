//
//  Double.swift
//  WeatherMap
//
//  Created by Saba Khutsishvili on 10/9/19.
//

import Foundation

extension Double {
    
    func toDate(format: String) -> String {
        let dateFormatter = DateFormatter()
//        let locale = Locale(identifier: "en_US")
        dateFormatter.dateFormat = format
//        dateFormatter.locale = locale
        let date = Date(timeIntervalSince1970: self)
        return dateFormatter.string(from: date)
    }
}
