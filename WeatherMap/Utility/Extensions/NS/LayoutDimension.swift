//
//  LayoutDimension.swift
//  WeatherMap
//
//  Created by Saba Khutsishvili on 10/8/19.
//

import UIKit


extension NSLayoutDimension {
    
    func equal(to constant: CGFloat) -> NSLayoutConstraint {
        return constraint(equalToConstant: constant)
    }
}
