//
//  NSObject.swift
//  WeatherMap
//
//  Created by Saba Khutsishvili on 10/7/19.
//

import Foundation

extension NSObject {
    
    var className: String {
        return String(describing: type(of: self))
    }
    
    class var className: String {
        return String(describing: self)
    }
}
