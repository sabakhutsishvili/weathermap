//
//  LayoutAnchor.swift
//  WeatherMap
//
//  Created by Saba Khutsishvili on 10/8/19.
//

import UIKit

extension NSLayoutAnchor {
    
    @objc func equal(to anchor: NSLayoutAnchor<AnchorType>) -> NSLayoutConstraint {
        return constraint(equalTo: anchor)
    }
}
