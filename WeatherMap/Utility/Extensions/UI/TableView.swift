//
//  TableView.swift
//  WeatherMap
//
//  Created by Saba Khutsishvili on 10/7/19.
//

import UIKit

extension UITableView {
    
    func registerCell<T>(_ type: T.Type) where T: UITableViewCell {
        register(
            UINib(nibName: type.className, bundle: nil),
            forCellReuseIdentifier: type.className
        )
    }
    
    func registerHeaderFooter<T>(_ type: T.Type) where T: UITableViewHeaderFooterView {
        register(
            UINib(nibName: type.className, bundle: nil),
            forHeaderFooterViewReuseIdentifier: type.className
        )
    }
    
    func dequeueCell<T>(_ type: T.Type, for indexPath: IndexPath) -> T where T: UITableViewCell {
        return dequeueReusableCell(withIdentifier: type.className, for: indexPath) as! T
    }
    
    func dequeueHeaderFooter<T>(_ type: T.Type) -> T where T: UITableViewHeaderFooterView {
        return dequeueReusableHeaderFooterView(withIdentifier: type.className) as! T
    }
}
