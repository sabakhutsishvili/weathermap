//
//  View.swift
//  WeatherMap
//
//  Created by Saba Khutsishvili on 10/8/19.
//

import UIKit

extension UIView {
    
    func fill(_ view: UIView) {
        translatesAutoresizingMaskIntoConstraints = false
        topAnchor.equal(to: view.topAnchor).isActive = true
        bottomAnchor.equal(to: view.bottomAnchor).isActive = true
        leadingAnchor.equal(to: view.leadingAnchor).isActive = true
        trailingAnchor.equal(to: view.trailingAnchor).isActive = true
    }
    
    func fillSuperview() {
        guard let superview = superview else { return }
        fill(superview)
    }
}
