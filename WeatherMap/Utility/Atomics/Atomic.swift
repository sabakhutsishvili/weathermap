//
//  Atomic.swift
//  WeatherMap
//
//  Created by Saba Khutsishvili on 10/7/19.
//

import Foundation

final class Atomic<Wrapped> {
    
    private let queue = DispatchQueue(label: "Atomic serial queue")
    private var _value: Wrapped
    
    init(_ value: Wrapped) {
        self._value = value
    }
    
    var value: Wrapped {
        get {
            return queue.sync { self._value }
        }
    }
    
    func mutate(_ transform: (inout Wrapped) -> ()) {
        queue.sync {
            transform(&self._value)
        }
    }
}
