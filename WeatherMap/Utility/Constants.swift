//
//  Constants.swift
//  WeatherMap
//
//  Created by Saba Khutsishvili on 10/7/19.
//

import UIKit

struct Constants {
    struct Error {
        struct Location {
            static let unavaiable = "Location Services is unavailable on your device"
            static let denied = "Location Services was denied. Please allow the app to use your location"
            static let restricted = "Location Services are restricted on your device"
            static let undetermined = "Please allow the app to use your location"
        }
    }
    
    struct Color {
        static let accent = #colorLiteral(red: 1, green: 0.8, blue: 0.003921568627, alpha: 1)
    }
    
    struct Image {
        struct Tab {
            static let today = #imageLiteral(resourceName: "tab_today")
            static let forecast = #imageLiteral(resourceName: "tab_forecast")
        }
    }
    
    struct Storyboard {
        static let main = "Main"
    }
    
    struct Key {
        struct Tab {
            static let today = "Today"
            static let forecast = "Forecast"
        }
        
        struct Keychain {
            static let service = "saba.khuts.WeatherMap.keychain"
            static let deviceUUIDKey = "deviceUUIDKey"
        }
    }
    
    struct Endpoint {
        struct WM {
            static let apiKey = "b07da2abebb981b5b3874956763fa118"
            static let baseURL = "api.openweathermap.org/data/2.5/"
            static var weatherURL: String { return "https://\(baseURL)weather?appid=\(apiKey)&units=metric" }
            static var forecastURL: String { return "https://\(baseURL)forecast?appid=\(apiKey)&units=metric" }
            
            static func iconURL(for name: String) -> String {
                return "https://openweathermap.org/img/wn/\(name)@2x.png"
            }
        }
    }
    
    struct Format {
        static let weekday = "EEEE"
        static let hourly = "HH:mm"
    }
}
