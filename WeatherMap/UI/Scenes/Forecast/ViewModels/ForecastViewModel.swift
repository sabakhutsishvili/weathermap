//
//  ForecastViewModel.swift
//  WeatherMap
//
//  Created by Saba Khutsishvili on 10/8/19.
//

import Foundation

struct ForecastViewModel {
    lazy var sectionModels = [ForecastSectionViewModel]()
    
    init() {
        sectionModels = []
    }
    
    init(from container: ForecastContainer) {
        for forecast in container.list {
            let weekday = forecast.dt.toDate(format: Constants.Format.weekday).uppercased()
            if let section = sectionModels.first(where: {$0.headerModel.title == weekday}) {
                section.cellModels.append(ForecastCellModel(forecast: forecast))
            } else {
                sectionModels.append(
                    ForecastSectionViewModel(
                        headerModel: ForecastHeaderModel(title: weekday),
                        cellModels: [ForecastCellModel(forecast: forecast)]
                    )
                )
            }
        }
    }
}
