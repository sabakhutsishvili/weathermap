//
//  ForecastSectionViewModel.swift
//  WeatherMap
//
//  Created by Saba Khutsishvili on 10/9/19.
//

import Foundation

final class ForecastSectionViewModel {
    var headerModel: ForecastHeaderModel
    lazy var cellModels = [ForecastCellModel]()
    
    init(
        headerModel: ForecastHeaderModel,
        cellModels: [ForecastCellModel]
    ) {
        self.headerModel = headerModel
        self.cellModels = cellModels
    }
    
    init(KVP: (key: String, value: [ForecastCellModel])) {
        self.headerModel = ForecastHeaderModel(title: KVP.key)
        self.cellModels = KVP.value
    }
}
