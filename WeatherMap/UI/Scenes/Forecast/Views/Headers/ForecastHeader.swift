//
//  ForecastHeader.swift
//  WeatherMap
//
//  Created by Saba Khutsishvili on 10/8/19.
//

import UIKit

final class ForecastHeader: UITableViewHeaderFooterView {
    
    @IBOutlet var headerLabel: UILabel!
    
    func configure(with model: ForecastHeaderModel) {
        headerLabel.text = model.title
    }
}

struct ForecastHeaderModel {
    let title: String
    
    init(title: String) {
        self.title = title.uppercased()
    }
}
