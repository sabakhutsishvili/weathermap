//
//  ForecastFactory.swift
//  WeatherMap
//
//  Created by Saba Khutsishvili on 10/7/19.
//

import UIKit

final class ForecastFactory {
    
    static var tabInstance: UIViewController {
        let configurator = ForecastConfigurator()
        let drawer = instantiate(ForecastDrawer.self, from: Constants.Storyboard.main)
        drawer.configurator = configurator
        drawer.tabBarItem = UITabBarItem(
            title: Constants.Key.Tab.forecast,
            image: Constants.Image.Tab.forecast,
            tag: 1
        )
        _ = drawer.view
        return UINavigationController(rootViewController: drawer)
    }
}
