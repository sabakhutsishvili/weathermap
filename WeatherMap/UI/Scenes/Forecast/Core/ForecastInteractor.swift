//
//  ForecastInteractor.swift
//  WeatherMap
//
//  Created by Saba Khutsishvili on 10/8/19.
//

import Foundation
import Alamofire

typealias ForecastCompletion = (Swift.Result<ForecastContainer, Error>) -> ()

protocol ForecastInteracting: Interactor {
    func loadForecast(for coordinates: Coordinate, completion: @escaping ForecastCompletion)
    func loadForecast(for city: String, completion: @escaping ForecastCompletion)
}

final class ForecastInteractor: ForecastInteracting {
    
    func loadForecast(for coordinates: Coordinate, completion: @escaping ForecastCompletion) {
        let parameters = [
            "lat": coordinates.lat,
            "lon": coordinates.lon
        ]
        request(
            Constants.Endpoint.WM.forecastURL,
            parameters: parameters
        ).responseData { [weak self] response in
            self?.parse(response, completion: completion)
        }
    }
    
    func loadForecast(for city: String, completion: @escaping ForecastCompletion) {
        let parameters = ["q": city]
        request(
            Constants.Endpoint.WM.forecastURL,
            parameters: parameters
        ).responseData { [weak self] response in
            self?.parse(response, completion: completion)
        }
    }
}
