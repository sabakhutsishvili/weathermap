//
//  ForecastConfigurator.swift
//  WeatherMap
//
//  Created by Saba Khutsishvili on 10/8/19.
//

import Foundation

typealias ForecastDisplaying = ForecastDrawing & HasScreenLoader & HasErrorView

protocol ForecastConfigurating: AnyObject {
    func setup(drawer: ForecastDisplaying)
}

final class ForecastConfigurator: ForecastConfigurating {
    
    func setup(drawer: ForecastDisplaying) {
        let interactor = ForecastInteractor()
        let director = ForecastDirector(drawer: drawer, interactor: interactor)
        drawer.director = director
    }
}
