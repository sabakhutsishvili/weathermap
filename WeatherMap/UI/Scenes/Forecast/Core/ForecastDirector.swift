//
//  ForecastDirector.swift
//  WeatherMap
//
//  Created by Saba Khutsishvili on 10/8/19.
//

import Foundation

protocol ForecastDirecting: AnyObject {
    var drawer: ForecastDisplaying { get }
    var interactor: ForecastInteracting { get }
    
    func loadForecastData()
}

final class ForecastDirector: ForecastDirecting {
    
    unowned let drawer: ForecastDisplaying
    let interactor: ForecastInteracting
    let locationManager: LocationManager
    
    init(
        drawer: ForecastDisplaying,
        interactor: ForecastInteracting,
        locationManager: LocationManager = .init()
    ) {
        self.drawer = drawer
        self.interactor = interactor
        self.locationManager = locationManager
        self.locationManager.delegate = self
    }
    
    func loadForecastData() {
        drawer.startScreenLoader()
        locationManager.requestLocation()
    }
}

extension ForecastDirector: LocationManagerDelegate {
    
    func locationManagerdidAllowLocation(_ sender: LocationManager) {
        self.loadForecastData()
    }
    
    func locationManager(_ sender: LocationManager, didRetrieve locationData: LocationData) {
        if let city = locationData.city {
            interactor.loadForecast(
                for: city,
                completion: { [weak self] result in
                    guard let self = self else { return }
                    DispatchQueue.main.async {
                        self.handleForecastData(result)
                    }
                }
            )
        } else {
            interactor.loadForecast(
                for: locationData.coordinates,
                completion: { [weak self] result in
                    guard let self = self else { return }
                    DispatchQueue.main.async {
                        self.handleForecastData(result)
                    }
                }
            )
        }
    }
    
    private func handleForecastData(_ result: Swift.Result<ForecastContainer, Error>) {
        switch result {
        case .success(let data):
            displayForecast(data)
        case .failure(let error):
            displayError(error.localizedDescription)
        }
    }
    
    private func displayForecast(_ container: ForecastContainer) {
        drawer.configure(with: ForecastViewModel(from: container))
    }
    
    func locationManager(_ sender: LocationManager, didFailWith failure: LocationManager.Failure) {
        displayError(failure.errorMessage)
    }
    
    private func displayError(_ message: String) {
        drawer.showErrorView(with: .init(errorMessage: message))
    }
}
