//
//  ForecastDrawer.swift
//  WeatherMap
//
//  Created by Saba Khutsishvili on 10/7/19.
//

import UIKit

protocol ForecastDrawing: UIViewController {
    var director: ForecastDirecting! { get set }
    var configurator: ForecastConfigurating! { get set }
    
    func configure(with viewModel: ForecastViewModel)
}

final class ForecastDrawer: BaseViewController, ForecastDrawing, HasErrorView {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var errorView: ErrorView!
    
    var director: ForecastDirecting!
    var configurator: ForecastConfigurating!
    
    var forecast = ForecastViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        configurator.setup(drawer: self)
        director.loadForecastData()
        setupTable()
        registerTableItems()
        addObserverToWillEnterForeground()
        errorView.delegate = self
    }
    
    func configure(with viewModel: ForecastViewModel) {
        stopScreenLoader()
        hideErrorView()
        forecast = viewModel
        tableView.reloadData()
    }
    
    @IBAction func handleRefresh(_ sender: UIBarButtonItem) {
        refresh()
    }
    
    @objc private func refresh() {
        hideErrorView()
        director.loadForecastData()
    }
    
    private func addObserverToWillEnterForeground() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(refresh),
            name: UIApplication.willEnterForegroundNotification,
            object: nil
        )
    }
    
    private func setupTable() {
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 64, bottom: 0, right: 0)
        tableView.separatorColor = UIColor(white: 0, alpha: 0.2)
    }
    
    private func registerTableItems() {
        tableView.registerHeaderFooter(ForecastHeader.self)
        tableView.registerCell(ForecastCell.self)
    }
}

extension ForecastDrawer: ErrorViewDelegate {
    
    func errorView(_ sender: ErrorView, didClickButton: UIButton) {
        refresh()
    }
    
    func showErrorView(with model: ErrorView.Model) {
        stopScreenLoader()
        configureErrorView(with: model)
    }
}

extension ForecastDrawer {
    struct Defaults {
        static let headerEstimatedHeight: CGFloat = 36
        static let rowEstimatedHeight: CGFloat = 36
    }
}
