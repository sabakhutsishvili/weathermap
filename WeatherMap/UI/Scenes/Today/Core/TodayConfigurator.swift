//
//  TodayConfigurator.swift
//  WeatherMap
//
//  Created by Saba Khutsishvili on 10/7/19.
//

import UIKit

typealias TodayDisplaying = TodayDrawing & HasScreenLoader & HasErrorView

protocol TodayConfigurating: AnyObject {
    func setup(drawer: TodayDisplaying)
}

final class TodayConfigurator: TodayConfigurating {
    
    func setup(drawer: TodayDisplaying) {
        let interactor = TodayInteractor()
        let director = TodayDirector(drawer: drawer, interactor: interactor)
        drawer.director = director
    }
}
