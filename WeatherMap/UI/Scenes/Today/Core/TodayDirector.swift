//
//  TodayDirector.swift
//  WeatherMap
//
//  Created by Saba Khutsishvili on 10/7/19.
//

import Foundation

protocol TodayDirecting: AnyObject {
    var drawer: TodayDisplaying { get }
    var interactor: TodayInteracting { get }
    
    func loadWeatherData()
}

final class TodayDirector: TodayDirecting {
    
    unowned let drawer: TodayDisplaying
    let interactor: TodayInteracting
    let databaseManager: DatabaseManager
    let locationManager: LocationManager
    
    init(
        drawer: TodayDisplaying,
        interactor: TodayInteracting,
        databaseManager: DatabaseManager = .init(),
        locationManager: LocationManager = .init()
    ) {
        self.drawer = drawer
        self.interactor = interactor
        self.databaseManager = databaseManager
        self.locationManager = locationManager
        self.locationManager.delegate = self
    }
    
    func loadWeatherData() {
        drawer.startScreenLoader()
        locationManager.requestLocation()
    }
}

extension TodayDirector: LocationManagerDelegate {
    
    func locationManagerdidAllowLocation(_ sender: LocationManager) {
        self.loadWeatherData()
    }
    
    func locationManager(_ sender: LocationManager, didRetrieve locationData: LocationData) {
        if let city = locationData.city {
            interactor.loadWeather(
                for: city,
                completion: { [weak self] result in
                    guard let self = self else { return }
                    DispatchQueue.main.async {
                        self.handleWeatherData(result)
                    }
                }
            )
        } else {
            interactor.loadWeather(
                for: locationData.coordinates,
                completion: { [weak self] result in
                    guard let self = self else { return }
                    DispatchQueue.main.async {
                        self.handleWeatherData(result)
                    }
                }
            )
        }
    }
    
    func locationManager(_ sender: LocationManager, didFailWith failure: LocationManager.Failure) {
        displayError(failure.errorMessage)
    }
    
    private func handleWeatherData(_ result: Swift.Result<WeatherContainer, Error>) {
        switch result {
        case .success(let data):
            displayWeather(data)
        case .failure(let error):
            displayError(error.localizedDescription)
        }
    }
    
    private func displayWeather(_ container: WeatherContainer) {
        storeWeatherData(from: container)
        drawer.configure(with:
            WeatherViewModel(
                iconName: container.weather.first!.icon,
                coordinates: container.coord,
                city: container.name,
                countryCode: container.sys.country,
                temperature: container.main.temp,
                mainTheme: container.weather.first!.main,
                cloudiness: container.clouds.all,
                humidity: container.main.humidity,
                pressure: container.main.pressure,
                windSpeed: container.wind.speed,
                windDirection: container.wind.deg
            )
        )
    }
    
    private func displayError(_ message: String) {
        drawer.showErrorView(with: .init(errorMessage: message))
    }
    
    private func storeWeatherData(from container: WeatherContainer) {
        databaseManager.store(
            weatherEntry: WeatherEntry(
                coordinates: container.coord,
                city: container.name,
                countryCode: container.sys.country,
                temperature: container.main.temp
            )
        )
    }
}
