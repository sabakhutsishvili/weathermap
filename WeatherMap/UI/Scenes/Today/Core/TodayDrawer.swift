//
//  TodayDrawer.swift
//  WeatherMap
//
//  Created by Saba Khutsishvili on 10/7/19.
//

import UIKit
import SDWebImage

protocol TodayDrawing: UIViewController {
    var director: TodayDirecting! { get set }
    var configurator: TodayConfigurating! { get set }
    
    func configure(with viewModel: WeatherViewModel)
}

final class TodayDrawer: BaseViewController, TodayDrawing, HasErrorView {
    
    @IBOutlet var shareBarButton: UIBarButtonItem!
    @IBOutlet var weatherStack: UIStackView!
    @IBOutlet var weatherImage: UIImageView!
    @IBOutlet var locationLabel: UILabel!
    @IBOutlet var weatherLabel: UILabel!
    @IBOutlet var detailsStack: UIStackView!
    @IBOutlet var cloudnessImage: UIImageView!
    @IBOutlet var cloudnessLabel: UILabel!
    @IBOutlet var humidityImage: UIImageView!
    @IBOutlet var humidityLabel: UILabel!
    @IBOutlet var celsiusImage: UIImageView!
    @IBOutlet var celsiusLabel: UILabel!
    @IBOutlet var windImage: UIImageView!
    @IBOutlet var windLabel: UILabel!
    @IBOutlet var directionImage: UIImageView!
    @IBOutlet var directionLabel: UILabel!
    @IBOutlet var errorView: ErrorView!
    
    var director: TodayDirecting!
    var configurator: TodayConfigurating!

    override func viewDidLoad() {
        super.viewDidLoad()
        configurator.setup(drawer: self)
        director.loadWeatherData()
        setStackSpacings()
        applyTheme()
        shareBarButton.isEnabled = false
        addObserverToWillEnterForeground()
        errorView.delegate = self
    }
    
    func configure(with viewModel: WeatherViewModel) {
        stopScreenLoader()
        hideErrorView()
        shareBarButton.isEnabled = true
        weatherImage.sd_setImage(with: URL(string: viewModel.iconURL)!, completed: nil)
        locationLabel.text = viewModel.place
        weatherLabel.text = viewModel.todayWeather
        cloudnessLabel.text = viewModel.cloudiness
        humidityLabel.text = viewModel.humidity
        celsiusLabel.text = viewModel.pressure
        windLabel.text = viewModel.windSpeed
        directionLabel.text = viewModel.windDirection
    }

    @IBAction func handleShare(_ sender: UIBarButtonItem) {
        let activity = UIActivityViewController(
            activityItems: ["\(locationLabel.text!), \(weatherLabel.text!)"],
            applicationActivities: nil
        )
        activity.popoverPresentationController?.barButtonItem = sender
        present(activity, animated: true, completion: nil)
    }
    
    @IBAction func handleRefresh(_ sender: UIBarButtonItem) {
        refresh()
    }
    
    @objc private func refresh() {
        hideErrorView()
        shareBarButton.isEnabled = false
        director.loadWeatherData()
    }
    
    private func addObserverToWillEnterForeground() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(refresh),
            name: UIApplication.willEnterForegroundNotification,
            object: nil
        )
    }
    
    private func applyTheme() {
        weatherImage.tintColor = Constants.Color.accent
        cloudnessImage.tintColor = Constants.Color.accent
        humidityImage.tintColor = Constants.Color.accent
        celsiusImage.tintColor = Constants.Color.accent
        windImage.tintColor = Constants.Color.accent
        directionImage.tintColor = Constants.Color.accent
    }
    
    private func setStackSpacings() {
        let spacing = UIScreen.main.bounds.height * 0.02
        weatherStack.spacing = spacing
        detailsStack.spacing = spacing * 2
    }
}

extension TodayDrawer: ErrorViewDelegate {
    
    func errorView(_ sender: ErrorView, didClickButton: UIButton) {
        refresh()
    }
    
    func showErrorView(with model: ErrorView.Model) {
        stopScreenLoader()
        configureErrorView(with: model)
    }
}
