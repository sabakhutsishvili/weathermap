//
//  TodayInteractor.swift
//  WeatherMap
//
//  Created by Saba Khutsishvili on 10/7/19.
//

import Foundation
import Alamofire

typealias WeatherCompletion = (Swift.Result<WeatherContainer, Error>) -> ()

protocol TodayInteracting: Interactor {
    func loadWeather(for coordinates: Coordinate, completion: @escaping WeatherCompletion)
    func loadWeather(for city: String, completion: @escaping WeatherCompletion)
}

final class TodayInteractor: TodayInteracting {
        
    func loadWeather(for coordinates: Coordinate, completion: @escaping WeatherCompletion) {
        let parameters = [
            "lat": coordinates.lat,
            "lon": coordinates.lon
        ]
        request(
            Constants.Endpoint.WM.weatherURL,
            parameters: parameters
        ).responseData { [weak self] response in
            self?.parse(response, completion: completion)
        }
    }
    
    func loadWeather(for city: String, completion: @escaping WeatherCompletion) {
        let parameters = ["q": city]
        request(
            Constants.Endpoint.WM.weatherURL,
            parameters: parameters
        ).responseData { [weak self] response in
            self?.parse(response, completion: completion)
        }
    }
}
