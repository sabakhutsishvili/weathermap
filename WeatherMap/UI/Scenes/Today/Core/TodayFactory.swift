//
//  TodayFactory.swift
//  WeatherMap
//
//  Created by Saba Khutsishvili on 10/7/19.
//

import UIKit

final class TodayFactory {
    
    static var tabInstance: UIViewController {
        let configurator = TodayConfigurator()
        let drawer = instantiate(TodayDrawer.self, from: Constants.Storyboard.main)
        drawer.configurator = configurator
        drawer.tabBarItem = UITabBarItem(
            title: Constants.Key.Tab.today,
            image: Constants.Image.Tab.today,
            tag: 0
        )
        return UINavigationController(rootViewController: drawer)
    }
}
