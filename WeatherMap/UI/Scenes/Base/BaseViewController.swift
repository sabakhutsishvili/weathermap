//
//  BaseViewController.swift
//  WeatherMap
//
//  Created by Saba Khutsishvili on 10/7/19.
//

import UIKit

class BaseViewController: UIViewController, HasScreenLoader {
    let screenLoader = ScreenLoader()
}
