//
//  ReusableView.swift
//  WeatherMap
//
//  Created by Saba Khutsishvili on 10/7/19.
//

import UIKit

class ReusableView: BaseView {
    
    @IBOutlet var contentView: UIView!
    
    override func commonInit() {
        super.commonInit()
        
        let bundle = Bundle(for: ReusableView.self)
        bundle.loadNibNamed(className, owner: self, options: nil)
        
        guard let content = contentView else {
            fatalError("contentView Not Set")
        }
        
        content.frame = self.bounds
        content.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        addSubview(content)
    }
}
