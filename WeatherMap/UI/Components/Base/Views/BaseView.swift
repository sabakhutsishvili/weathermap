//
//  BaseView.swift
//  WeatherMap
//
//  Created by Saba Khutsishvili on 10/7/19.
//

import UIKit

class BaseView: UIView {
    
    convenience init() {
        self.init(frame: CGRect.zero)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        
    }
}
