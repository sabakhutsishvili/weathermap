//
//  ErrorView.swift
//  WeatherMap
//
//  Created by Saba Khutsishvili on 10/8/19.
//

import UIKit

protocol ErrorViewDelegate: AnyObject {
    func errorView(_ sender: ErrorView, didClickButton: UIButton)
}

final class ErrorView: ReusableView {
    
    @IBOutlet var iconImage: UIImageView!
    @IBOutlet var textLabel: UILabel!
    @IBOutlet var actionButton: UIButton!
    
    weak var delegate: ErrorViewDelegate?
    
    override func commonInit() {
        super.commonInit()
        actionButton.layer.cornerRadius = 4
    }
    
    func configure(with model: ErrorView.Model) {
        iconImage.image = model.icon
        textLabel.text = model.errorMessage
    }
    
    @IBAction func actionButtonClicked(_ sender: UIButton) {
        delegate?.errorView(self, didClickButton: sender)
    }
}
