//
//  ErrorViewModel.swift
//  WeatherMap
//
//  Created by Saba Khutsishvili on 10/8/19.
//

import UIKit

extension ErrorView {
    
    struct Model {
        let icon: UIImage = #imageLiteral(resourceName: "data_load_error")
        let errorMessage: String
    }
}
