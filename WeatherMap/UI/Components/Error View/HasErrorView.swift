//
//  HasErrorView.swift
//  WeatherMap
//
//  Created by Saba Khutsishvili on 10/8/19.
//

import UIKit

protocol HasErrorView: AnyObject {
    var errorView: ErrorView! { get set }
    
    func showErrorView(with model: ErrorView.Model)
    func hideErrorView()
}

extension HasErrorView where Self: UIViewController & HasScreenLoader {
    
    func configureErrorView(with model: ErrorView.Model) {
        errorView.configure(with: model)
        view.bringSubviewToFront(errorView)
        errorView.isHidden = false
    }
    
    func hideErrorView() {
        view.sendSubviewToBack(errorView)
        errorView.isHidden = true
    }
}

