//
//  AccentImageView.swift
//  WeatherMap
//
//  Created by Saba Khutsishvili on 10/7/19.
//

import UIKit

final class AccentImageView: BaseImageView {
    
    override func commonInit() {
        super.commonInit()
        tintColor = Constants.Color.accent
    }
}
