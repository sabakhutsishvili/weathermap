//
//  ScreenLoader.swift
//  WeatherMap
//
//  Created by Saba Khutsishvili on 10/7/19.
//

import UIKit

final class ScreenLoader {
    
    private var lock = Atomic<Int>(0)
    private var containerView = UIView()
    private var isLoading = false
    
    private let visualEffectsView: UIVisualEffectView = {
        let blurEffect = UIBlurEffect(style: .regular)
        let effectView = UIVisualEffectView(effect: blurEffect)
        return effectView
    }()
    
    func start(on view: UIView, at position: CGPoint) {
        guard isLoading == false else { return }
        isLoading = true
        setup(on: view, at: position)
        view.addSubview(containerView)
    }
    
    func stop() {
        guard isLoading else { return }
        isLoading = false
        containerView.removeFromSuperview()
    }
    
    func setup(on view: UIView, at position: CGPoint) {
        containerView.frame = view.frame
        containerView.addSubview(self.visualEffectsView)
        visualEffectsView.fillSuperview()
        
        let spinner = UIActivityIndicatorView(style: .whiteLarge)
        spinner.color = .darkGray
        
        containerView.addSubview(spinner)
        spinner.center = containerView.center
        spinner.startAnimating()
    }
}
