//
//  HasScreenLoader.swift
//  WeatherMap
//
//  Created by Saba Khutsishvili on 10/8/19.
//

import UIKit

protocol HasScreenLoader: AnyObject {
    var screenLoader: ScreenLoader { get }
    
    func startScreenLoader()
    func stopScreenLoader()
}

extension HasScreenLoader where Self: UIViewController {
    
    func startScreenLoader() {
        screenLoader.start(on: view, at: view.center)
    }
    
    func stopScreenLoader() {
        screenLoader.stop()
    }
}
