//
//  LocationManager+Error.swift
//  WeatherMap
//
//  Created by Saba Khutsishvili on 10/8/19.
//

import Foundation

extension LocationManager {
    
    enum Failure: Error {
        case locationServiceUnavailable
        case locationRetrieveFailed(String)
        case authorizationFailed(LocationAuthorizationProblem)
        
        var errorMessage: String {
            switch self {
            case .locationServiceUnavailable:
                return Constants.Error.Location.unavaiable
            case .locationRetrieveFailed(let localizedError):
                return localizedError
            case .authorizationFailed(let status):
                return status.errorMessage
            }
        }
        
        enum LocationAuthorizationProblem {
            case denied
            case restricted
            case underermined
            
            var errorMessage: String {
                switch self {
                case .denied:
                    return Constants.Error.Location.denied
                case .restricted:
                    return Constants.Error.Location.restricted
                case .underermined:
                    return Constants.Error.Location.undetermined
                }
            }
        }
    }
}
