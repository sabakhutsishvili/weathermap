//
//  LocationManager.swift
//  WeatherMap
//
//  Created by Saba Khutsishvili on 10/8/19.
//

import CoreLocation

protocol LocationManagerDelegate: AnyObject {
    func locationManagerdidAllowLocation(_ sender: LocationManager)
    func locationManager(_ sender: LocationManager, didRetrieve locationData: LocationData)
    func locationManager(_ sender: LocationManager, didFailWith failure: LocationManager.Failure)
}

final class LocationManager: NSObject {
    
    private let manager = CLLocationManager()
    private var locationRetrieved = false
    
    static let `default` = LocationManager()
    weak var delegate: LocationManagerDelegate?
    
    var locationData: LocationData?
    
    override init() {
        super.init()
        setupLocationManager()
    }
    
    private func setupLocationManager() {
        manager.delegate = self
        manager.requestWhenInUseAuthorization()
        manager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        manager.distanceFilter = kCLDistanceFilterNone
        manager.activityType = .other
    }
    
    func requestLocation() {
        locationRetrieved = false
        manager.requestLocation()
    }
}

extension LocationManager: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            delegate?.locationManagerdidAllowLocation(self)
        case .denied:
            delegate?.locationManager(self, didFailWith: .authorizationFailed(.denied))
        case .restricted:
            delegate?.locationManager(self, didFailWith: .authorizationFailed(.restricted))
        case .notDetermined:
            delegate?.locationManager(self, didFailWith: .authorizationFailed(.underermined))
        @unknown default:
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard locationRetrieved == false else { return }
        if let location = locations.first {
            locationRetrieved = true
            getPlacemark(
                for: location,
                completion: { [weak self] placemark in
                    guard let self = self else { return }
                    let locationData = LocationData(location: location, placemark: placemark)
                    self.locationData = locationData
                    self.delegate?.locationManager(self, didRetrieve: locationData)
                }
            )
        } else {
            delegate?.locationManager(self, didFailWith: .locationServiceUnavailable)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        let clError = CLError(_nsError: error as NSError)
        delegate?.locationManager(self, didFailWith: .locationRetrieveFailed(clError.localizedDescription))
    }
}

extension LocationManager {
    
    var authorizationStatus: CLAuthorizationStatus {
        return CLLocationManager.authorizationStatus()
    }
    
    var locationEnabled: Bool {
        return CLLocationManager.locationServicesEnabled()
    }
}
