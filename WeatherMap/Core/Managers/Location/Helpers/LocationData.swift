//
//  LocationData.swift
//  WeatherMap
//
//  Created by Saba Khutsishvili on 10/8/19.
//

import CoreLocation

struct LocationData {
    let location: CLLocation
    var placemark: CLPlacemark?
    
    var coordinates: Coordinate {
        return Coordinate(
            lat: location.coordinate.latitude,
            lon: location.coordinate.longitude
        )
    }
    
    var city: String? {
        return placemark?.locality
    }
}
