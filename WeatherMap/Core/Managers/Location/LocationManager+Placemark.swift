//
//  LocationManager+Placemark.swift
//  WeatherMap
//
//  Created by Saba Khutsishvili on 10/8/19.
//

import Foundation
import CoreLocation

extension LocationManager {

    func getPlacemark(
        for location: CLLocation,
        completion: @escaping (CLPlacemark?) -> ()
    ) {
        CLGeocoder().reverseGeocodeLocation(
            location,
            completionHandler: { placemarks, error in
                guard
                    error == nil,
                    let placemark = placemarks?[0]
                    else { return completion(nil) }
                
                completion(placemark)
            }
        )
    }
}
