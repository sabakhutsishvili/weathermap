//
//  DatabaseManager.swift
//  WeatherMap
//
//  Created by Saba Khutsishvili on 10/9/19.
//

import UIKit
import KeychainAccess
import FirebaseDatabase

final class DatabaseManager {
    
    private let deviceUUID: String
    private let databaseReference = Database.database().reference()
    
    init() {
        let keychain = Keychain(service: Constants.Key.Keychain.service)
        if let storedUUID = try? keychain.getString(Constants.Key.Keychain.deviceUUIDKey) {
            deviceUUID = storedUUID
        } else {
            deviceUUID = UIDevice.current.identifierForVendor!.uuidString
            try? keychain.set(deviceUUID, key: Constants.Key.Keychain.deviceUUIDKey)
        }
    }
    
    func store(weatherEntry: WeatherEntry) {
        if let weatherJSON = weatherEntry.toJSON {
            let timestamp = Int64(Date().timeIntervalSince1970 * 1000).description
            databaseReference.child(deviceUUID).child(timestamp).setValue(weatherJSON)
        }
    }
}

struct WeatherEntry: Codable {
    let coordinates: Coordinate
    let city: String
    let countryCode: String
    let temperature: Double
}
