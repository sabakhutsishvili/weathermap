//
//  Forecast.swift
//  WeatherMap
//
//  Created by Saba Khutsishvili on 10/8/19.
//

import Foundation

struct ForecastContainer: Codable {
    let cod: String
    let message: Double
    let cnt: Int
    let list: [Forecast]
    let city: City
}

struct Forecast: Codable {
    let dt: Double
    let main: Main
    let weather: [Weather]
    let clouds: Clouds
}

struct City: Codable {
    let name: String
    let coord: Coordinate
    let country: String
}
