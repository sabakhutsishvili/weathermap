//
//  Models.swift
//  WeatherMap
//
//  Created by Saba Khutsishvili on 10/7/19.
//

import Foundation

struct WeatherContainer: Codable {
    let coord: Coordinate
    let weather: [Weather]
    let base: String
    let main: Main
    let wind: Wind
    let clouds: Clouds
    let dt: Int64
    let sys: Sys
    let id: Int64
    let name: String
    let cod: Int
}

struct Coordinate: Codable {
    let lat: Double
    let lon: Double
}

struct Weather: Codable {
    let id: Int64
    let main: String
    let description: String
    let icon: String
}

struct Main: Codable {
    let temp: Double
    let pressure: Double
    let humidity: Int
}

struct Wind: Codable {
    let speed: Double
    let deg: Int?
}

struct Clouds: Codable {
    let all: Int
}

struct Sys: Codable {
    let message: Double
    let country: String
}
